class SuperMap {
  constructor() {

    // init
    this.init()

    // init var
    this.mapbox = new Object

  }

  init: function() {
    mapboxgl.accessToken = 'pk.eyJ1IjoibWFpcm9kIiwiYSI6ImNpdWppNXk4dzAwb3Iyem9kNHFxMjlqaWcifQ.NfXPR5t9xv8WvbDTUUWpaA';
    var this.mapbox = new mapboxgl.Map({
      container: 'map', // container id
      style: 'mapbox://styles/mapbox/streets-v9', //stylesheet location
      center: [-74.50, 40], // starting position
      zoom: 9 // starting zoom
    });

    console.log('init OK');
  }

}
