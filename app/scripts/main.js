var global = {};
global.positions = [];
dataInit();

mapboxgl.accessToken = 'pk.eyJ1IjoibWFpcm9kIiwiYSI6ImNpdWppNXk4dzAwb3Iyem9kNHFxMjlqaWcifQ.NfXPR5t9xv8WvbDTUUWpaA';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [11.6593173929, 48.0527807276],
    zoom: 20,
    maxZoom: 50
});

map.on('load', function () {
    map.addSource("route", {
        "type": "geojson",
        "data": {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": global.positions
            }
        }
    });

    map.addLayer({
        "id": "route",
        "type": "line",
        "source": "route",
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#FF0000",
            "line-width": 2
        }
    });
});
